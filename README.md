# todoMVC

This project contains cypress test for the given assignment.

# How to run the cypress tests in local desktop?
<br>Step1: git clone https://gitlab.com/spsaurabhpandey/todomvc.git
<br>Step2: open the contents of this repository in an IDE (ex: VS Code).
<br>Step3: Run following commands in terminal
<br>a) npm install
<br>b) npx cypress open -> choose your browser and execute todo.feature file.

# Where can I find scenario(s) in Gherkin?
<br>The feature files are at cypress/integration/

# Where and how can I find the report of test Execution?
<br><pre>Option 1) In CICD pipeline of this project, the report is generated as an artifact. 
Click on the three dots next to the pipeline and download the report.
         Or
Option 2) When you are running these tests from your local machine, then:
    Run following commands:
        a) "npx cypress run" or "npm run headlessChrome"
        b) npm run report
Latest html report will be placed at cypress/cucumber-report/index.html </pre>

# Where can I find the documents of given Assignment, defects found etc.?
<br>./docs/Test automation assignment* pdf file
<br>./docs/defects.docx
