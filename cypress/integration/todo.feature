Feature: As a user of the todo app, I should be able to add, edit, delete and mark todo items as complete.

    Background: : user visits the todo app
        Given The user visits the todo app
        Then The title and header of the page should be displayed

    # 6 Positive Scenarios are listed below
    Scenario: Add a todo item
        When User adds "2" todo item in the list
        Then The todo count should be "2"

    Scenario: Delete a todo item
        Given There are "3" items in the todo list
        When User hovers over a todoItem and clicks on red cross
        Then The todo count should be "2"

    Scenario: Mark a todo item complete
        Given There are "3" items in the todo list
        When User check marks a todo item
        Then The todo item should be striked Off and todoCount should show "2" items left

    Scenario: Clear completed items
        Given There are "4" items in the todo list
        When User check marks a todo item
        And Clicks on Clear completed
        Then The todo count should be "3"

    Scenario Outline: Edit a todo item
        Given There are "2" items in the todo list
        When User edits the first todo item to "<editedValue>"
        Then User should be able to see the "<editedValue>" in first todo item
        Examples:
            | editedValue                    |
            | I want to do xyz thing tomorow |

    # 04 Negative scenarios are listed below
    Scenario: Blank todo item should not be created
        When User presses enter without text in the textbox - what needs to be done?
        Then Todo item should not be created

    Scenario: Replacing an existing todo item text with blank value
        Given There are "1" items in the todo list
        When User edits the first todo item to ""
        Then Todo item should not be created

    Scenario: Refreshing the URL after adding the todo items
        Given There are "2" items in the todo list
        When User refreshes the URL
        Then Todo item list should not change

    Scenario Outline: Refreshing the URL after editing a todo item
        Given There are "2" items in the todo list
        When User edits the first todo item to "<editedValue>"
        And User refreshes the URL
        Then User should be able to see the "<editedValue>" in first todo item
        Examples:
            | editedValue                    |
            | I want to do xyz thing tomorow |